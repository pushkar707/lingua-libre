from django.core.management.base import BaseCommand
from social_django.models import UserSocialAuth
from requests_oauthlib import OAuth1Session
from lingualibre.settings import SOCIAL_AUTH_MEDIAWIKI_KEY, SOCIAL_AUTH_MEDIAWIKI_SECRET, MEDIA_ROOT
from upload_batches.models import Recording
import uuid
import re


class Command(BaseCommand):
    help = "Uploads the specified pending Recording to Wikimedia Commons"

    def add_arguments(self, parser):
        parser.add_argument("recording_id", type=int)
        # TODO: add --dry-run argument

    def handle(self, *args, **options):
        if not options["recording_id"]:
            self.stderr.write("Please provide recording id")
            return

        recording_id = options["recording_id"]

        # get Recording
        record = Recording.objects.get(id=recording_id)

        if (record.state != Recording.State.PENDING):
            print('Recording already uploaded')
            return

        # get user social auth
        usersocialauth = UserSocialAuth.objects.get(
            user_id=record.batch.locutor.linked_user)

        oauth_token = usersocialauth.extra_data.get(
            'access_token').get('oauth_token')
        oauth_token_secret = usersocialauth.extra_data.get(
            'access_token').get('oauth_token_secret')

        # create OAuth 1.0a client
        client = OAuth1Session(
            SOCIAL_AUTH_MEDIAWIKI_KEY,
            client_secret=SOCIAL_AUTH_MEDIAWIKI_SECRET,
            resource_owner_key=oauth_token,
            resource_owner_secret=oauth_token_secret
        )

        try:
            # Retrieve the CSRF token from the Commons
            url = 'https://commons.wikimedia.beta.wmflabs.org/w/api.php?action=query&meta=tokens&format=json&formatversion=2'
            # TODO: Handle errors (403...)
            csrf_token = client.get(url).json()['query']['tokens']['csrftoken']

            file_name = "LL-" + record.batch.language_qid + "-" + \
                record.batch.locutor.name + "-" + record.transcription + ".wav"
            print(str(MEDIA_ROOT) + '/' + str(record.file))
            upload_url = 'https://commons.wikimedia.beta.wmflabs.org/w/api.php?action=upload&format=json&formatversion=2'
            files = {
                'file': (file_name, open(str(MEDIA_ROOT) + '/' + str(record.file), 'rb'), 'audio/wav'),
            }

            payload = {
                'token': csrf_token,
                'filename': file_name,
                'text': '== {{int:filedesc}} ==\nLingua Libre test record.\nFeel free to delete at some point.\n\n== {{int:license-header}} ==\n' + record.batch.locutor.license,
                'comment': 'Uploading a pronunciation file with Lingua Libre'
            }

            # Beta Wikidata values for SDC: P694(instance of) Q626670(pronunciation file)
            # Cf https://github.com/wikimedia/labs-tools-Isa/blob/master/isa/src/participation-manager/populate.js

            response = client.post(upload_url, payload, files=files).json()
            print(response)
            error = response.get('error', None)
            result = response.get('upload', {}).get('result', None)

            if error or result != 'Success':
                record.state = Recording.State.ERRORED
                if error:
                    errorInfo = error['info']
                    if re.match(r'File extension "\.[^"]*" does not match the detected MIME type of the file \(image/[^)]*\).', errorInfo):
                        record.error = 'filetype-banned'
                    elif errorInfo == 'The file name you are currently trying to import to contains invalid filename characters.':
                        record.error = 'illegal-filename'
                    elif errorInfo == 'The file name you are currently trying to import to is too long.':
                        record.error = 'filenameerror-toolong'
                    else:
                        record.error = 'unknown error'
                elif result:
                    if (response['upload']['warnings'].get('exists', None)):
                        record.error = 'Duplicate-recording'
            else:
                record.state = Recording.State.UPLOADED
            record.save()
        except Exception as e:
            print(e)
