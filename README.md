# Lingua Libre

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=lingua-libre_lingua-libre&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=lingua-libre_lingua-libre)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=lingua-libre_lingua-libre&metric=sqale_index)](https://sonarcloud.io/summary/new_code?id=lingua-libre_lingua-libre)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=lingua-libre_lingua-libre&metric=coverage)](https://sonarcloud.io/summary/new_code?id=lingua-libre_lingua-libre)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=lingua-libre_lingua-libre&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=lingua-libre_lingua-libre)

**Lingua Libre** is an online collaborative project and tool, which aims to build a collaborative, multilingual, audiovisual speech corpus under a free license ([English Wikipedia](https://en.wikipedia.org/wiki/Lingua_Libre)).

This repository contains both the backend (Django) and frontend (Vue) code of the Lingua Libre website.

## Technical documentation
Technical documentation (architecture, database models, API routes) is available in the [`doc/`](doc) folder.
It will help you grasp all the various concepts that pertain to Lingua Libre's inner workings.

The backend exposes a public REST API that is used by the frontend and that you can also use for your project.
The REST API definition is available at [`doc/openapi.yaml`](doc/openapi.yaml).

## Installation

This tutorial covers all the steps required to install and run Lingua Libre, but is geared towards using this installation as a local development environment.

**If you wish to deploy Lingua Libre in a production environement**, we maintain Ansible playbooks in the [`operations` repository](https://gitlab.wikimedia.org/repos/wikimedia-france/lingua-libre/operations).
You can either use them with Ansible, or read through them as some sort of step-by-step tutorials to deploy and/or update Lingua Libre.

### Via Docker
You must have docker installed on your machine for this, and logged in to docker CLI.

#### Steps
1. Add a **config.ini** file. 
  * `cp config.ini.sample config.ini`
  * Add the OAuth key and secret
  * Configure the database settings (host, port, name, user, password) with the credentials you created for the MariaDB database.
  * (if serving the website on the web) change the Django secret key.

2. **Update `docker-compose.yml` file.** After setting up your credentials in the `config.ini` file, you need to add the same credentials to the `docker-compose.yml` file for the MariaDB service. Look for the environment key under mariadb service. You will find comments guiding you further

3. Make sure there's nothing running on port 8080, 8079 and 3306, then run the command: `docker compose up`
4. You will have backend service running on port 8080, frontend on port 8079, and database on port 3306.

### Manually

#### Dependencies
Ensure Python, NodeJS and the MariaDB Django connector are available.

```bash
# Install Python 3.10.x
# Install Node 18+
# Install MariaDB
# Additional tools:
apt install build-essential default-libmysqlclient-dev pkg-config
```

#### Create the MariaDB database
1. Create a database (default: `lingualibre`)
2. Create a MariaDB user (default: `lingualibre@localhost`)
3. Grant all privileges on this database to the user

#### `config.ini`
1. `cp config.ini.sample config.ini`
2. Add the OAuth key and secret
3. Configure the database settings (host, port, name, user, password) with the credentials you created for the MariaDB database.
4. (if serving the website on the web) change the Django secret key.

#### Install
```bash
python -m venv .venv`
source .venv/bin/activate
pip install -r requirements.txt
python manage.py migrate
cd front-end/ && npm clean-install
```

#### Launch!
Ensure ports 8080 and 8079 –currently hardcoded– are free, then:

```bash
python manage.py runserver 8080
cd front-end/ && npm run dev
```

## Support
Found a bug? Want a feature ? Please open a ticket on : 
- [https://phabricator.wikimedia.org/project/view/6913/](https://phabricator.wikimedia.org/project/view/6913/).

## Contribute
### Write code
#### Front-end
When contributing code to the Vue front-end, do remember to run the following commands, as they automatically apply the code style guidelines and make sure your code does not introduce any new issues:

```
npm run format    # Apply the code style guidelines
npm run lint:fix  # Run the linter and fix issues that can be automatically fixed
npm run test:unit # Run unit tests
```

Writing unit tests for Components you created is *highly* recommended. Ideally, the front-end should have 100% coverage at some point.

The front-end currently does not have integration (or end-to-end) tests.
If you have any knowledge in designing or writing such tests, please reach out!

### Run tests
#### Back-end
Give the db user all privileges on the `test_lingualibre` (or a database where you prepend `test_` to your db's name) database (do not create it).

(in the venv, FOR COVERAGE REPORT)
1. `coverage erase`
2. `coverage run manage.py test`
3. `coverage report`

Otherwise
`python manage.py test`

## License
- AGPL v3, Wikimédia France & contributors.

### Licenses of used media
* [Lingua Libre logo no text](https://commons.wikimedia.org/wiki/File:Lingualibre-logo-no-text.svg), CC BY-SA 4.0 Marc Brouillon, Keymap9, 0x010C.
* [Wikimedia France logo - horizontal](https://commons.wikimedia.org/wiki/File:Wikimedia_France_logo_-_horizontal.svg), CC BY-SA 4.0 Ash Crow, Wikimedia Foundation Trademark Policy.

## Project status

### Lingua Libre Django Alpha
This repository is a major revamp initiated in 2023 by Wikimedia France thank to DGLFLF's sponsorship. Wikimedia France hired Florian Cuny to set the technical direction and create a proof of concept. University of Toulouse/URFIST Occitanie's Wikimedian in résidence Hugo Lopez lead the Google Summer of Code 2024 and with Florian Cuny onboarded an intern. Google-Wikimedia Foundation's intern Pushkar Bansal built upon existing assets and pushed for this Alpha release at https://dev.lingualibre.org .


## Sponsors
| Years | Logo | Sponsor
|:----|:----:|:----|
| 2016-present | <img width="64" src="https://upload.wikimedia.org/wikipedia/commons/7/75/Wikimedia_France_logo.svg" alt="Wikimedia France logo linking to its website." /> | <a href="https://www.wikimedia.fr">Wikimedia France</a>
| 2016-present | <img width="64" src="https://upload.wikimedia.org/wikipedia/commons/1/1c/Ministère_de_la_Culture.svg" alt="Wikimedia France logo linking to its website." /> | <a href="https://www.culture.gouv.fr/Thematiques/langue-francaise-et-langues-de-france">DGLFLF (Délégation Générale à la Langue Française et aux Langues de France)</a>, a unit of the French Ministry of Culture. **(Gold sponsor)**
| 2018 | <img width="64" src="https://upload.wikimedia.org/wikipedia/commons/3/31/Wikimedia_Foundation_logo_-_vertical.svg" alt="Wikimedia logo linking to its website."> | <a href="https://meta.wikimedia.org">Wikimedia Foundation</a>
| 2023-2024 | <img width="64" src="https://upload.wikimedia.org/wikipedia/commons/c/c0/URFIST_Occitanie-2023.svg" alt="URFIST Occitanie."> | <a href="https://urfist.univ-toulouse.fr">URFIST Occitanie</a>
| 2024 | <img width="64" src="https://raw.githubusercontent.com/scribe-org/Organization/main/resources/images/logos/GSoCLogo.png" alt="Google Summer of Code logo linking to its website."> | <a href="https://summerofcode.withgoogle.com/">Google Summer of Code</a>

## Contributors
- Nicolas Vion (200x-2016), Wikimedia France's hire in 2016.
- Antoine Lamielle (2016-20), Wikimedia Foundation's grantee 2018, URFIST hire 2023.
- Florian Cuny (2016-20), Wikimédia France hire 2023-24.
- Hugo Lopez (2013-present), University of Toulouse's hire, Wikimedian in residence 2023-24.
- Pushkar Bansal (2024), Google Summer of Code intern.

…and about three dozens more occasional contributors to various coding aspects of this languages documentation open source project. See https://github.com/lingua-libre/ 's multiple repositories and their respective contributors.
