import { describe, it, expect, afterEach } from 'vitest'
import { mount } from '@vue/test-utils'
import DeletionDialog from './DeletionDialog.vue'

describe('Button to open the dialog', () => {
  afterEach(() => {
    // We need to manually clean up the body HTML code between unit tests
    // due to CdxDialog's use of <Teleport>
    document.body.outerHTML = ''
  })

  it('should be there', () => {
    const wrapper = mount(DeletionDialog, {
      props: { confirmationText: 'abcd', nameOfTheThingToDelete: 'locutor' },
    })
    expect(wrapper.find('button')).toBeTruthy()
  })

  it('should open the dialog when clicked', async () => {
    const wrapper = mount(DeletionDialog, {
      props: { confirmationText: 'abcd', nameOfTheThingToDelete: 'locutor' },
    })

    // no dialog should be visible at that point - i.e. in the DOM
    // CdxDialog uses Vue's Teleport, so we need to check for 'cdx-dialog' class in the document's body.
    // wrapper has no idea where the dialog actually is.
    expect(document.body.getElementsByClassName('cdx-dialog').length).toBe(0)

    await wrapper.getComponent('button').trigger('click')

    // there should be a div with 'cdx-dialog' class in the DOM now
    expect(document.body.getElementsByClassName('cdx-dialog').length).toBe(1)
  })
})

describe.todo('Dialog', () => {
  // TODO: add unit tests for the dialog logic
  // I don't know how to test it easily due to the use of Vue's Teleport.
})
