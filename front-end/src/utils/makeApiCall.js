import axios from 'axios'

const NETWORK_ERROR = 'NETWORK_ERROR'
const NOT_AUTHENTICATED = 'NOT_AUTHENTICATED'
const UNEXPECTED_ERROR = 'UNEXPECTED_ERROR'

export default async (
  method,
  url,
  errorHandler,
  withCredentials = true,
  data = {},
  headers = {},
  params = {},
) => {
  try {
    const response = await axios({
      method: method,
      url: url,
      data: data,
      withCredentials,
      headers,
      params,
    })

    console.log(response.data)
    return response.data
  } catch (error) {
    if (error.response) {
      if (error.response.status === axios.HttpStatusCode.Forbidden) {
        errorHandler(NOT_AUTHENTICATED)
      } else {
        errorHandler(UNEXPECTED_ERROR)
        console.log(error.response)
      }
    } else if (error.request) {
      errorHandler(NETWORK_ERROR)
    }
  }
}
