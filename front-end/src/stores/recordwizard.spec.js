import { describe, it, expect, beforeEach } from 'vitest'
import { setActivePinia, createPinia } from 'pinia'
import { useRecordWizardStore } from './recordwizard'

describe('RecordWizard store', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  describe('locutor getter', () => {
    it('should return the correct locutor depending on the selectedLocutorId', () => {
      const store = useRecordWizardStore()

      // very basic "locutors" with only their id and a name
      const LOCUTOR_1 = { id: 1, name: 'poslovitch' }
      const LOCUTOR_2 = { id: 2, name: '0x010c' }
      const LOCUTOR_3 = { id: 4, name: 'lyokoi' }
      store.locutors = [LOCUTOR_1, LOCUTOR_2, LOCUTOR_3]

      store.selectedLocutorId = undefined
      expect(store.locutor).toBeUndefined()

      store.selectedLocutorId = LOCUTOR_1.id
      expect(store.locutor).toStrictEqual(LOCUTOR_1)

      store.selectedLocutorId = LOCUTOR_3.id
      expect(store.locutor).toStrictEqual(LOCUTOR_3)

      store.selectedLocutorId = 9999
      expect(store.locutor).toBeUndefined()
    })

    it('should return undefined if there are no locutors', () => {
      const store = useRecordWizardStore()
      store.selectedLocutorId = undefined
      store.locutors = []

      expect(store.locutor).toBeUndefined()
    })
  })

  describe('hasRecordings getter', () => {
    it('should return false if there are no recordings recorded', () => {
      const store = useRecordWizardStore()
      store.recordings = {}

      expect(store.hasRecordings).toBeFalsy()
    })

    it('should return true if there are some recordings recorded', () => {
      const store = useRecordWizardStore()
      store.recordings = {
        'my word': 1, // some bogus data, what we really need here is to have at least a key-value pair
      }

      expect(store.hasRecordings).toBeTruthy()
    })
  })

  describe('availableLanguages getter', () => {
    it('should return [] if no locutor is selected', () => {
      const store = useRecordWizardStore()
      store.locutors = [
        { id: 1, languages_used: ['french'] },
        { id: 2, languages_used: ['english'] },
        { id: 3, languages_used: ['esperanto'] },
      ]
      store.selectedLocutorId = undefined

      expect(store.availableLanguages).toStrictEqual([])
    })

    it('should return the languages of the selected locutor', () => {
      const store = useRecordWizardStore()
      store.locutors = [
        { id: 1, languages_used: ['french'] },
        { id: 2, languages_used: ['english'] },
        { id: 3, languages_used: ['esperanto'] },
      ]
      store.selectedLocutorId = 1

      expect(store.availableLanguages).toStrictEqual(['french'])
    })
  })

  describe('removeSelectedLocutor action', () => {
    it('should do nothing if no locutor is selected', () => {
      const store = useRecordWizardStore()
      store.locutors = [{ id: 1 }, { id: 2 }, { id: 3 }]
      store.selectedLocutorId = undefined

      store.removeSelectedLocutor()

      expect(store.locutors).toStrictEqual([{ id: 1 }, { id: 2 }, { id: 3 }])
    })

    it.each([
      [1, 2], // delete locutor 1, so next locutor on the list that should be automatically selected is 2
      [2, 3],
      [3, 2], // delete locutor 3, there is no locutor next to it in the list, so we have to take the previous one: locutor 2
    ])(
      'should delete locutor with id %i and the new selected locutor id should be %i',
      (locutorIdToDelete, newSelectedLocutorId) => {
        const store = useRecordWizardStore()
        store.locutors = [{ id: 1 }, { id: 2 }, { id: 3 }]
        store.selectedLocutorId = locutorIdToDelete

        store.removeSelectedLocutor()

        expect(store.locutors.length).toBe(2)
        expect(store.locutors.find((item) => item.id === locutorIdToDelete)).toBeUndefined()
        expect(store.selectedLocutorId).toBe(newSelectedLocutorId)
      },
    )
  })

  describe('addWords action', () => {
    it('should do nothing if input is empty', () => {
      const store = useRecordWizardStore()
      store.words = []

      store.addWords('')
      expect(store.words).toStrictEqual([])
    })

    it('should do nothing if input is only empty spaces', () => {
      const store = useRecordWizardStore()
      store.words = []

      store.addWords('   ')
      expect(store.words).toStrictEqual([])
    })

    it('should do nothing if input is ###', () => {
      const store = useRecordWizardStore()
      store.words = []

      store.addWords('###')
      expect(store.words).toStrictEqual([])
    })

    it('should add a single word', () => {
      const store = useRecordWizardStore()
      store.words = []

      store.addWords('poisson')
      expect(store.words).toStrictEqual(['poisson'])
    })

    it('should trim the word of useless spaces', () => {
      const store = useRecordWizardStore()
      store.words = []

      store.addWords('   poisson  ')
      expect(store.words).toStrictEqual(['poisson'])
    })

    it('should trim the word of tabulations', () => {
      const store = useRecordWizardStore()
      store.words = []

      store.addWords('\tpoisson')
      expect(store.words).toStrictEqual(['poisson'])
    })

    it('should add multiple words, separated with #', () => {
      const store = useRecordWizardStore()
      store.words = []

      store.addWords('chant#des#partisans')
      expect(store.words).toStrictEqual(['chant', 'des', 'partisans'])
    })

    it('should not add words that are already in the words list', () => {
      const store = useRecordWizardStore()
      store.words = ['pays']

      store.addWords('paysan#pays#paysage')
      expect(store.words).toStrictEqual(['pays', 'paysan', 'paysage'])
    })

    it('should ignore duplicate words in the input', () => {
      const store = useRecordWizardStore()
      store.words = []

      store.addWords('carte#carte#carton')
      expect(store.words).toStrictEqual(['carte', 'carton'])
    })
  })
})
