import { defineStore } from 'pinia'
import axios from 'axios'
import VueCookies from 'vue-cookies'
import makeApiCall from '../utils/makeApiCall'

export const useRecordWizardStore = defineStore('record-wizard', {
  state: () => ({
    locutors: [],
    selectedLocutorId: null,
    selectedLanguageId: null,
    hasSelectedNewLocuter: null,
    newLocuter: {
      name: '',
      is_main_locutor: false,
      license: 'cc-zero',
      can_be_deleted: false,
      languages_used: [],
      place_of_residence: '',
    },
    words: [],
    recordings: {},
    apiCallError: '',
    shareableListId: undefined
  }),
  getters: {
    locutor: (state) => {
      return state.hasSelectedNewLocuter
        ? state.newLocuter
        : state.locutors.find((locutor) => locutor.id === state.selectedLocutorId)
    },
    hasRecordings: (state) => Object.values(state.recordings).length > 0,
    availableLanguages() {
      if (!this.locutor) return []
      return this.locutor['languages_used']
    },
  },
  actions: {
    removeSelectedLocutor() {
      if (!this.locutor) return

      let selectedLocutorIndex = this.locutors.findIndex((item) => item.id === this.locutor.id)
      this.locutors.splice(selectedLocutorIndex, 1)

      if (selectedLocutorIndex >= this.locutors.length) {
        selectedLocutorIndex = this.locutors.length - 1
      }

      this.selectedLocutorId = this.locutors[selectedLocutorIndex].id
    },
    addLanguage() {
      const locutor = this.hasSelectedNewLocuter
        ? this.newLocuter
        : this.locutors.find((locutor) => locutor.id === this.selectedLocutorId)
      if (locutor) {
        locutor.languages_used.push({ proficiency: 2, language_qid: '', place_of_learning: '' })
      }
    },
    removeLanguage(qid) {
      const locutor = this.hasSelectedNewLocuter
        ? this.newLocuter
        : this.locutors.find((locutor) => locutor.id === this.selectedLocutorId)
      if (locutor) {
        locutor.languages_used = locutor.languages_used.filter(
          (language) => language.language_qid !== qid,
        )
      }
    },
    saveLocuter() {
      if (this.hasSelectedNewLocuter) {
        makeApiCall(
          'post',
          '/locutors/my',
          (error) => {
            this.apiCallError = error
          },
          true,
          this.locutor,
          {
            'X-CSRFToken': VueCookies.get('csrftoken'),
          },
          {}
        )
      } else {
        makeApiCall(
          'PUT',
          '/locutors/my/' + this.selectedLocutorId,
          (error) => {
            this.apiCallError = error
          },
          true,
          this.locutor,
          {
            'X-CSRFToken': VueCookies.get('csrftoken'),
          },
        )
      }
    },
    addWords(input) {
      // Split by '#'
      const words = input.split('#')
      for (const word of words) {
        // Trim the word
        const trimmedWord = word.replace(/\t/g, '').trim()
        // ensure the word is valid
        if (trimmedWord === '') continue

        // ensure the word is not already in the list
        if (this.words.includes(trimmedWord)) continue

        // add to the list
        this.words.push(trimmedWord)
      }
    },
    addWordsArray(input) {
      this.words = this.words.concat(input)
    },
  },
})
