from django.db import models
from django.core.exceptions import BadRequest
from upload_batches.models import UploadBatch


class Locutor(models.Model):
    name = models.CharField(max_length=256)
    linked_user = models.ForeignKey(
        "user_profile.User", on_delete=models.DO_NOTHING, related_name="locutors")
    gender = models.CharField(max_length=32, blank=True)
    place_of_residence = models.CharField(max_length=32, blank=True)
    is_main_locutor = models.BooleanField(default=False)
    LICENSE_CHOICES = [
        ("cc-zero", "CC0"),
        ("cc-by", "CC BY 4.0"),
        ("cc-by-sa", "CC BY-SA 4.0"),
    ]
    license = models.CharField(
        max_length=16, choices=LICENSE_CHOICES, default="cc-by-sa")

    @property
    def can_be_deleted(self):
        # TODO: Find a way to check in WCQS if this locutor has recordings associated to it
        # because it may have no upload batches pending here, but recordings already uploaded to Commons

        # Check if this locutor has any upload batches
        upload_batches = UploadBatch.objects.filter(locutor=self.id)

        return not self.is_main_locutor and upload_batches.count() == 0

    def delete(self, *args, **kwargs):
        if self.can_be_deleted:
            super().delete(*args, **kwargs)
        else:
            raise BadRequest("This Locutor cannot be deleted")


class LanguageProficiency(models.IntegerChoices):
    BEGINNER = 1, 'Beginner'
    AVERAGE = 2, 'Average'
    GOOD = 3, 'Good'
    PROFESSIONAL = 4, 'Professional'
    NATIVE = 5, 'Native'


class UsesLanguage(models.Model):
    locutor = models.ForeignKey(
        "locutor", on_delete=models.CASCADE, related_name="languages_used")
    language_qid = models.CharField(max_length=32)
    proficiency = models.SmallIntegerField(choices=LanguageProficiency.choices)
    place_of_learning = models.CharField(max_length=32, blank=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=[
                                    'locutor', 'language_qid'], name="cannot be twice the same locutor and language")
        ]


class ShareableLists(models.Model):
    words = models.TextField()
    language_qid = models.CharField(max_length=32)
    locutor = models.ForeignKey(
        "locutor", on_delete=models.DO_NOTHING, related_name="lists_created")
    created_on = models.DateTimeField(auto_now_add=True)
